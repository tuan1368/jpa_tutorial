package com.jpa.app;

import com.jpa.service.StudentCRUDService;
import com.jpa.service.StudentQueryService;
import com.jpa.service.StudentService;

/**
 * 
 * @author tuannguyen
 *
 */
public class Application {
	static StudentService studentService = new StudentService();
	static StudentCRUDService studentCRUDService = new StudentCRUDService();
	static StudentQueryService studentQueryService = new StudentQueryService();

	public static void main(String[] args) {

//      CRUD Operator
		studentService.createStudentAndProfile();
		studentService.createStudentAndAddress();
		studentService.createStudentAndGroup();
		studentCRUDService.insertStudent();
		studentCRUDService.selectStudent();
		studentCRUDService.updateStudent();
		studentCRUDService.deleteStudent();
//		Using JPQL
		studentQueryService.selectStudentUsingJPQL();
		studentQueryService.selectStudentNameParameterUsingJPQL();
		studentQueryService.getStudentUsingNamedQuery();

		studentQueryService.updateStudentUsingJPQL();
		studentQueryService.deleteStudentUsingJPQL();
		studentQueryService.getStudentByCallStoredProcedure(24,158);
//		Using criteria
		studentQueryService.criteriaSelect();
		studentQueryService.criteriaWhere();
		studentQueryService.criteriaConditionBinding();
		studentQueryService.criteriaOrderByAge();
		studentQueryService.criteriaGroupByAge();
		
	}
	
}
