package com.jpa.service;

import com.jpa.model.Group;
import com.jpa.util.JPAUtil;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import java.util.List;

public class GroupService {

	public void getStudent() {

		EntityManagerFactory entityFactory = JPAUtil.getEntityManagerFactory();
		EntityManager em = entityFactory.createEntityManager();
		try {
			em.getTransaction().begin();
			TypedQuery<Group> query = em.createNamedQuery("findById", Group.class);
			query.setParameter("i1", 3);
			query.setParameter("i2", 6);
			List<Group> groupList = query.getResultList();
			System.out.println("Group size: " + groupList.size());
			groupList.stream().forEach(c->System.out.println(c));
			em.getTransaction().commit();

		} catch (Exception ex) {
			System.out.println(ex.toString());
		}
		em.close();
		entityFactory.close();

	}

}
