package com.jpa.service;

import com.jpa.model.Group;
import com.jpa.util.JPAUtil;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public class GroupQueryService {

	public void selectGroupUsingJPQL() {

		EntityManagerFactory entityFactory = JPAUtil.getEntityManagerFactory();
		EntityManager em = entityFactory.createEntityManager();
		try {
			em.getTransaction().begin();

			System.out.println("-- All groups --");
			Query query = em.createQuery("SELECT g FROM Group g");
			List<Group> resultList = query.getResultList();
			System.out.println("Groups count: " + resultList.size());
			resultList.stream().forEach(g -> System.out.println(g));

			em.getTransaction().commit();

		} catch (Exception e) {
			System.out.println(e.toString());
		}
		em.close();
		entityFactory.close();
	}

	public void selectGroupNameParameterUsingJPQL() {
		EntityManagerFactory entityFactory = JPAUtil.getEntityManagerFactory();
		EntityManager em = entityFactory.createEntityManager();
		try {
			em.getTransaction().begin();

			System.out.println("-- All groups --");
			Query query = em.createQuery("SELECT g FROM Group g where name like: name");
			query.setParameter("name", "Group 1");
			List<Group> resultList = query.getResultList();
			System.out.println("Groups count: " + resultList.size());
			resultList.stream().forEach(g -> System.out.println(g));

			em.getTransaction().commit();

		} catch (Exception e) {
			System.out.println(e.toString());
		}
		em.close();
		entityFactory.close();
	}

	public void getGroupUsingNamedQuery() {
		EntityManagerFactory entityFactory = JPAUtil.getEntityManagerFactory();
		EntityManager em = entityFactory.createEntityManager();
		try {
			em.getTransaction().begin();
			TypedQuery<Group> query = em.createNamedQuery("findById", Group.class);
			query.setParameter("i1", 3);
			query.setParameter("i2", 6);
			List<Group> groupList = query.getResultList();
			System.out.println("Group size: " + groupList.size());
			groupList.stream().forEach(g -> System.out.println(g));
			em.getTransaction().commit();

		} catch (Exception ex) {
			System.out.println(ex.toString());
		}

		em.close();
		entityFactory.close();

	}

	@SuppressWarnings("unchecked")
	public void getGroupByCallStoredProcedure(int id1, int id2) {
		EntityManagerFactory entityFactory = JPAUtil.getEntityManagerFactory();
		EntityManager em = entityFactory.createEntityManager();
		StoredProcedureQuery query = em.createNamedStoredProcedureQuery("findByIdProcedure");
		try {
			em.getTransaction().begin();
			query.setParameter("ID1", id1);
			query.setParameter("ID2", id2);
			List<Group> groupList = query.getResultList();
			System.out.println("Group size: " + groupList.size());
			groupList.stream().forEach(g -> System.out.println(g));
			em.getTransaction().commit();

		} catch (Exception ex) {
			System.out.println(ex.toString());
		}
		em.close();
		entityFactory.close();

	}

	public void updateGroupUsingJPQL() {
		EntityManagerFactory entityFactory = JPAUtil.getEntityManagerFactory();
		EntityManager em = entityFactory.createEntityManager();
		try {
			em.getTransaction().begin();

			System.out.println("-- All groups --");
			Query query = em.createQuery("UPDATE Group g SET g.name =: name where g.id >=: id");
			query.setParameter("name", "Group updated");
			query.setParameter("id", 3);
			int rowsUpdated = query.executeUpdate();
			System.out.println("entities Updated: " + rowsUpdated);

			em.getTransaction().commit();

		} catch (Exception e) {
			System.out.println(e.toString());
		}
		em.close();
		entityFactory.close();
	}

	public void deleteGroupUsingJPQL() {
		EntityManagerFactory entityFactory = JPAUtil.getEntityManagerFactory();
		EntityManager em = entityFactory.createEntityManager();
		try {
			em.getTransaction().begin();

			System.out.println("-- Delete groups --");
			Query query = em.createQuery("DELETE from Group g where g.id =: id");
			query.setParameter("id", 165);
			int rowsUpdated = query.executeUpdate();
			System.out.println("entities deleted: " + rowsUpdated);

			em.getTransaction().commit();

		} catch (Exception e) {
			System.out.println(e.toString());
		}
		em.close();
		entityFactory.close();
	}

	public void criteriaSelect() {
		EntityManagerFactory entityFactory = JPAUtil.getEntityManagerFactory();
		EntityManager em = entityFactory.createEntityManager();
		try {
			em.getTransaction().begin();
			CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
			CriteriaQuery<Group> criteriaQuery = criteriaBuilder.createQuery(Group.class);
			Root<Group> rootGroup = criteriaQuery.from(Group.class);

			criteriaQuery.select(rootGroup.get("name"));

			CriteriaQuery<Group> select = criteriaQuery.select(rootGroup);
			TypedQuery<Group> query = em.createQuery(select);
			query.getResultList().stream().forEach(g -> System.out.println(g));
			em.getTransaction().commit();

		} catch (Exception e) {
			System.out.println(e.toString());
		}
		em.close();
		entityFactory.close();
	}

	public void criteriaWhere() {
		EntityManagerFactory entityFactory = JPAUtil.getEntityManagerFactory();
		EntityManager em = entityFactory.createEntityManager();
		try {
			em.getTransaction().begin();
			CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
			CriteriaQuery<Group> criteriaQuery = criteriaBuilder.createQuery(Group.class);
			Root<Group> rootGroup = criteriaQuery.from(Group.class);
			

			criteriaQuery.where(criteriaBuilder.like(rootGroup.get("name"), "Java%"));

			CriteriaQuery<Group> select = criteriaQuery.select(rootGroup);
			TypedQuery<Group> query = em.createQuery(select);
			query.getResultList().stream().forEach(g -> System.out.println(g));
			em.getTransaction().commit();

		} catch (Exception e) {
			System.out.println(e.toString());
		}
		em.close();
		entityFactory.close();
	}

	public void criteriaConditionBinding() {
		EntityManagerFactory entityFactory = JPAUtil.getEntityManagerFactory();
		EntityManager em = entityFactory.createEntityManager();
		try {
			em.getTransaction().begin();
			CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
			CriteriaQuery<Group> criteriaQuery = criteriaBuilder.createQuery(Group.class);
			Root<Group> rootGroup = criteriaQuery.from(Group.class);

			javax.persistence.criteria.Predicate predicateId = criteriaBuilder.equal(rootGroup.get("id"), 170);
			javax.persistence.criteria.Predicate predicateName = criteriaBuilder.like(rootGroup.get("name"),
					"Java Group");
			javax.persistence.criteria.Predicate predicateFinal = criteriaBuilder.or(predicateId, predicateName);

			CriteriaQuery<Group> select = criteriaQuery.select(rootGroup);
			criteriaQuery.where(predicateFinal);
			TypedQuery<Group> query = em.createQuery(criteriaQuery);
			query.getResultList().stream().forEach(g -> System.out.println(g));
			em.getTransaction().commit();

		} catch (Exception e) {
			System.out.println(e.toString());
		}
		em.close();
		entityFactory.close();
	}
	
	public void criteriaOrderBy() {
		EntityManagerFactory entityFactory = JPAUtil.getEntityManagerFactory();
		EntityManager em = entityFactory.createEntityManager();
		try {
			em.getTransaction().begin();
			CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
			CriteriaQuery<Group> criteriaQuery = criteriaBuilder.createQuery(Group.class);
			Root<Group> rootGroup = criteriaQuery.from(Group.class);
			
			criteriaQuery.orderBy(criteriaBuilder.desc(rootGroup.get("id")));


			CriteriaQuery<Group> select = criteriaQuery.select(rootGroup);
			TypedQuery<Group> query = em.createQuery(select);
			query.getResultList().stream().forEach(g -> System.out.println(g));
			em.getTransaction().commit();

		} catch (Exception e) {
			System.out.println(e.toString());
		}
		em.close();
		entityFactory.close();
	}
	
	public void criteriaGroupBy() {
		EntityManagerFactory entityFactory = JPAUtil.getEntityManagerFactory();
		EntityManager em = entityFactory.createEntityManager();
		try {
			em.getTransaction().begin();
			CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
			CriteriaQuery<Object[]> criteriaQuery = criteriaBuilder.createQuery(Object[].class);
			Root<Group> rootGroup = criteriaQuery.from(Group.class);
			
			criteriaQuery.multiselect(rootGroup.get("name"), criteriaBuilder.count(rootGroup)).groupBy(rootGroup.get("name"));
			System.out.print("Group name");  
			System.out.println("\t Count");  
			em.createQuery(criteriaQuery).getResultList().stream().forEach(object -> System.out.println(object[0] + "\t" + object[1]));
			em.getTransaction().commit();

		} catch (Exception e) {
			System.out.println(e.toString());
		}
		em.close();
		entityFactory.close();
	}

}
