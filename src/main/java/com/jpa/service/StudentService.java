package com.jpa.service;

import com.jpa.model.Address;
import com.jpa.model.Group;
import com.jpa.model.Profile;
import com.jpa.model.Student;
import com.jpa.util.JPAUtil;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class StudentService {

	public StudentService() {

	}

	public void insertStudent() {

		EntityManagerFactory entityFactory = JPAUtil.getEntityManagerFactory();
		EntityManager em = entityFactory.createEntityManager();

		Student student = new Student("tuanna", 26);
		try {
			em.getTransaction().begin();
			em.persist(student);
			em.getTransaction().commit();
			
		} catch(Exception ex) {
			System.out.println(ex.toString());
		}
		em.close();
		entityFactory.close();
	}
	
	public void createStudentAndProfile() {

		EntityManagerFactory entityFactory = JPAUtil.getEntityManagerFactory();
		EntityManager em = entityFactory.createEntityManager();
		
		Profile profile = new Profile("Tuan", "0911011TS");
		Student student = new Student("TONY",22, profile);
		try {
			em.getTransaction().begin();
			em.persist(profile);
			em.persist(student);
			em.getTransaction().commit();
			
		} catch(Exception ex) {
			System.out.println(ex.toString());
		}
		em.close();
		entityFactory.close();
		
	}
	
	public void createStudentAndAddress() {

		EntityManagerFactory entityFactory = JPAUtil.getEntityManagerFactory();
		EntityManager em = entityFactory.createEntityManager();
		List<Address> addresses = new ArrayList<>();
		addresses.add(new Address("QUAN 5", "NGUYEN VAN CU"));
		addresses.add(new Address("QUAN 1", "TRAN HUNG DAO"));
		Student student = new Student("TONY", 24, addresses);
		try {
			em.getTransaction().begin();
			for(Address address : addresses) {
				em.persist(address);
			}
			em.persist(student);
			em.getTransaction().commit();
			
		} catch(Exception ex) {
			System.out.println(ex.toString());
		}
		em.close();
		entityFactory.close();
		
	}
	
	public void createStudentAndGroup() {

		EntityManagerFactory entityFactory = JPAUtil.getEntityManagerFactory();
		EntityManager em = entityFactory.createEntityManager();
		
		Student student1 = new Student();
		Student student2 = new Student();
		Group group1 = new Group("Group 1");
		Group group2 = new Group("Group 2");
		Set<Student> students = new HashSet<>();
		students.add(student1);
		students.add(student2);
		Set<Group> groups = new HashSet<>();
		groups.add(group1);
		groups.add(group2);
		student1.setName("Student1");
		student2.setName("Student2");
		
		student1.setGroups(groups);
		student1.setAge(25);
		student2.setGroups(groups);
		student2.setAge(26);
		
		group1.setStudents(students);
		group2.setStudents(students);
		
		
		try {
			em.getTransaction().begin();
			
			em.persist(student1);
			em.persist(student2);
			
			em.persist(group1);
			em.persist(group2);
			em.getTransaction().commit();
			
		} catch(Exception ex) {
			System.out.println(ex.toString());
		}
		em.close();
		entityFactory.close();
		
	}
}
