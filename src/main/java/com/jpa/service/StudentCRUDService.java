package com.jpa.service;

import com.jpa.model.Student;
import com.jpa.util.JPAUtil;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

public class StudentCRUDService {
	
	public void insertStudent() {
		EntityManagerFactory entityFactory = JPAUtil.getEntityManagerFactory();
		EntityManager em = entityFactory.createEntityManager();
		try {
			em.getTransaction().begin();
			Student student1 = new Student("Herry", 21);
			Student student2 = new Student("Diana", 22);
			em.persist(student1);
			em.persist(student2);
			em.getTransaction().commit();

		} catch (Exception e) {
			System.out.println(e.toString());
		}
		em.close();
		entityFactory.close();
	}

	public void selectStudent() {
		EntityManagerFactory entityFactory = JPAUtil.getEntityManagerFactory();
		EntityManager em = entityFactory.createEntityManager();
		try {
			em.getTransaction().begin();
			Student student = em.find(Student.class, 3);
			System.out.println(student);

			em.getTransaction().commit();

		} catch (Exception e) {
			System.out.println(e.toString());
		}
		em.close();
		entityFactory.close();
	}

	public void updateStudent() {
		EntityManagerFactory entityFactory = JPAUtil.getEntityManagerFactory();
		EntityManager em = entityFactory.createEntityManager();
		try {
			em.getTransaction().begin();
			Student student = em.find(Student.class, 13);
			student.setName("TonyX");
			student.setAge(23);
			em.getTransaction().commit();
			System.out.println(student);

		} catch (Exception e) {
			System.out.println(e.toString());
		}
		em.close();
		entityFactory.close();
	}

	public void deleteStudent() {
		EntityManagerFactory entityFactory = JPAUtil.getEntityManagerFactory();
		EntityManager em = entityFactory.createEntityManager();
		try {
			em.getTransaction().begin();
			Student student = em.find(Student.class, 13);
			em.remove(student);
			em.getTransaction().commit();

		} catch (Exception e) {
			System.out.println(e.toString());
		}
		em.close();
		entityFactory.close();
	}

}
