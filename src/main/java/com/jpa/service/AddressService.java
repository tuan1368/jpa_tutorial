package com.jpa.service;

import com.jpa.model.Address;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class AddressService {
	private static EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("JPA-herbinate");

	public AddressService() {

	}

	public void insertAddress(Address address) {
		EntityManager em = entityManagerFactory.createEntityManager();
		em.getTransaction().begin();
		em.persist(address);
		em.getTransaction().commit();
		em.close();
	}

}
