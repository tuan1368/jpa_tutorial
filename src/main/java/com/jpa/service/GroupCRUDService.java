package com.jpa.service;

import com.jpa.model.Group;
import com.jpa.util.JPAUtil;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

public class GroupCRUDService {

	public void insertGroup() {
		EntityManagerFactory entityFactory = JPAUtil.getEntityManagerFactory();
		EntityManager em = entityFactory.createEntityManager();
		try {
			em.getTransaction().begin();
			Group javaGroup = new Group("Java group");
			Group jpaGroup = new Group("JPA group");
			em.persist(javaGroup);
			em.persist(jpaGroup);
			em.getTransaction().commit();

		} catch (Exception e) {
			System.out.println(e.toString());
		}
		em.close();
		entityFactory.close();
	}

	public void selectGroup() {
		EntityManagerFactory entityFactory = JPAUtil.getEntityManagerFactory();
		EntityManager em = entityFactory.createEntityManager();
		try {
			em.getTransaction().begin();
			Group group = em.find(Group.class, 1);
			System.out.println(group);

			em.getTransaction().commit();

		} catch (Exception e) {
			System.out.println(e.toString());
		}
		em.close();
		entityFactory.close();
	}

	public void updateGroup() {
		EntityManagerFactory entityFactory = JPAUtil.getEntityManagerFactory();
		EntityManager em = entityFactory.createEntityManager();
		try {
			em.getTransaction().begin();
			Group group = em.find(Group.class, 170);
			group.setName("New Group");
			em.getTransaction().commit();
			System.out.println(group);

		} catch (Exception e) {
			System.out.println(e.toString());
		}
		em.close();
		entityFactory.close();
	}

	public void deleteGroup() {
		EntityManagerFactory entityFactory = JPAUtil.getEntityManagerFactory();
		EntityManager em = entityFactory.createEntityManager();
		try {
			em.getTransaction().begin();
			Group group = em.find(Group.class, 1);
			em.remove(group);
			em.getTransaction().commit();

		} catch (Exception e) {
			System.out.println(e.toString());
		}
		em.close();
		entityFactory.close();
	}

}
