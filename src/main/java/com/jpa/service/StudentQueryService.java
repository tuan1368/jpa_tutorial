package com.jpa.service;

import com.jpa.model.Student;
import com.jpa.util.JPAUtil;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public class StudentQueryService {
	
	public void selectStudentUsingJPQL() {

		EntityManagerFactory entityFactory = JPAUtil.getEntityManagerFactory();
		EntityManager em = entityFactory.createEntityManager();
		try {
			em.getTransaction().begin();

			System.out.println("-- All students --");
			Query query = em.createQuery("SELECT s FROM Student s");
			List<Student> resultList = query.getResultList();
			System.out.println("Students count: " + resultList.size());
			resultList.stream().forEach(s -> System.out.println(s));

			em.getTransaction().commit();

		} catch (Exception e) {
			System.out.println(e.toString());
		}
		em.close();
		entityFactory.close();
	}
	
	public void selectStudentNameParameterUsingJPQL() {
		EntityManagerFactory entityFactory = JPAUtil.getEntityManagerFactory();
		EntityManager em = entityFactory.createEntityManager();
		try {
			em.getTransaction().begin();

			System.out.println("-- Select student --");
			Query query = em.createQuery("SELECT s FROM Student s where name like: name");
			query.setParameter("name", "TONY");
			List<Student> resultList = query.getResultList();
			System.out.println("Students count: " + resultList.size());
			resultList.stream().forEach(s -> System.out.println(s));

			em.getTransaction().commit();

		} catch (Exception e) {
			System.out.println(e.toString());
		}
		em.close();
		entityFactory.close();
	}
	
	public void getStudentUsingNamedQuery() {
		EntityManagerFactory entityFactory = JPAUtil.getEntityManagerFactory();
		EntityManager em = entityFactory.createEntityManager();
		try {
			em.getTransaction().begin();
			TypedQuery<Student> query = em.createNamedQuery("findById", Student.class);
			query.setParameter("i1", 3);
			query.setParameter("i2", 10);
			List<Student> groupList = query.getResultList();
			System.out.println("Student size: " + groupList.size());
			groupList.stream().forEach(s -> System.out.println(s));
			em.getTransaction().commit();

		} catch (Exception ex) {
			System.out.println(ex.toString());
		}

		em.close();
		entityFactory.close();

	}
	
	public void updateStudentUsingJPQL() {
		EntityManagerFactory entityFactory = JPAUtil.getEntityManagerFactory();
		EntityManager em = entityFactory.createEntityManager();
		try {
			em.getTransaction().begin();

			System.out.println("-- Update Student --");
			Query query = em.createQuery("UPDATE Student s SET s.name =: name, s.age =: age where s.id >=: id");
			query.setParameter("name", "Victoria");
			query.setParameter("id", 16);
			query.setParameter("age", 30);
			int rowsUpdated = query.executeUpdate();
			System.out.println("entities Updated: " + rowsUpdated);

			em.getTransaction().commit();

		} catch (Exception e) {
			System.out.println(e.toString());
		}
		em.close();
		entityFactory.close();
	}

	public void deleteStudentUsingJPQL() {
		EntityManagerFactory entityFactory = JPAUtil.getEntityManagerFactory();
		EntityManager em = entityFactory.createEntityManager();
		try {
			em.getTransaction().begin();

			System.out.println("-- Delete student --");
			Query query = em.createQuery("DELETE from Student s where s.id =: id");
			query.setParameter("id", 123);
			int rowsUpdated = query.executeUpdate();
			System.out.println("entities deleted: " + rowsUpdated);

			em.getTransaction().commit();

		} catch (Exception e) {
			System.out.println(e.toString());
		}
		em.close();
		entityFactory.close();
	}

	@SuppressWarnings("unchecked")
	public void getStudentByCallStoredProcedure(int _age, int _id) {
		EntityManagerFactory entityFactory = JPAUtil.getEntityManagerFactory();
		EntityManager em = entityFactory.createEntityManager();
		StoredProcedureQuery query = em.createNamedStoredProcedureQuery("findByAgeProcedure");
		try {
			em.getTransaction().begin();
			query.setParameter("s_age", _age);
			query.setParameter("s_id", _id);
			query.execute();
			List<Student> studentList = query.getResultList();
			System.out.println("Student size: " + studentList.size());
			System.out.println("Name" + "\t" + "Age");
			studentList.stream().forEach(s -> System.out.println(s));
			System.out.println("ProfileName");
			String name = (String)query.getOutputParameterValue("p_name");
			System.out.println(name);
			em.getTransaction().commit();

		} catch (Exception ex) {
			System.out.println(ex.toString());
		}
		em.close();
		entityFactory.close();

	}
	
	public void criteriaSelect() {
		EntityManagerFactory entityFactory = JPAUtil.getEntityManagerFactory();
		EntityManager em = entityFactory.createEntityManager();
		try {
			em.getTransaction().begin();
			CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
			CriteriaQuery<Student> criteriaQuery = criteriaBuilder.createQuery(Student.class);
			Root<Student> rootStudent = criteriaQuery.from(Student.class);

			criteriaQuery.select(rootStudent.get("name"));

			CriteriaQuery<Student> select = criteriaQuery.select(rootStudent);
			TypedQuery<Student> query = em.createQuery(select);
			System.out.println("Name"+ "\t Age");
			query.getResultList().stream().forEach(s -> System.out.println(s));
			em.getTransaction().commit();

		} catch (Exception e) {
			System.out.println(e.toString());
		}
		em.close();
		entityFactory.close();
	}

	public void criteriaWhere() {
		EntityManagerFactory entityFactory = JPAUtil.getEntityManagerFactory();
		EntityManager em = entityFactory.createEntityManager();
		try {
			em.getTransaction().begin();
			CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
			CriteriaQuery<Student> criteriaQuery = criteriaBuilder.createQuery(Student.class);
			Root<Student> rootStudent = criteriaQuery.from(Student.class);
			

			criteriaQuery.where(criteriaBuilder.like(rootStudent.get("name"), "Victo%"));

			CriteriaQuery<Student> select = criteriaQuery.select(rootStudent);
			TypedQuery<Student> query = em.createQuery(select);
			List<Student> studentList = query.getResultList();
			System.out.println("Student size: " + studentList.size());
			for(Student student :studentList) {
				System.out.println(student.getName());
			}
//			
			em.getTransaction().commit();

		} catch (Exception e) {
			System.out.println(e.toString());
		}
		em.close();
		entityFactory.close();
	}

	public void criteriaConditionBinding() {
		EntityManagerFactory entityFactory = JPAUtil.getEntityManagerFactory();
		EntityManager em = entityFactory.createEntityManager();
		try {
			em.getTransaction().begin();
			CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
			CriteriaQuery<Student> criteriaQuery = criteriaBuilder.createQuery(Student.class);
			Root<Student> rootStudent = criteriaQuery.from(Student.class);

			javax.persistence.criteria.Predicate predicateId = criteriaBuilder.equal(rootStudent.get("id"), 19);
			javax.persistence.criteria.Predicate predicateName = criteriaBuilder.like(rootStudent.get("name"),
					"Victoria%");
			javax.persistence.criteria.Predicate predicateFinal = criteriaBuilder.and(predicateId, predicateName);

			CriteriaQuery<Student> select = criteriaQuery.select(rootStudent);
			criteriaQuery.where(predicateFinal);
			TypedQuery<Student> query = em.createQuery(criteriaQuery);
			query.getResultList().stream().forEach(g -> System.out.println(g));
			em.getTransaction().commit();

		} catch (Exception e) {
			System.out.println(e.toString());
		}
		em.close();
		entityFactory.close();
	}
	
	public void criteriaOrderByAge() {
		EntityManagerFactory entityFactory = JPAUtil.getEntityManagerFactory();
		EntityManager em = entityFactory.createEntityManager();
		try {
			em.getTransaction().begin();
			CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
			CriteriaQuery<Student> criteriaQuery = criteriaBuilder.createQuery(Student.class);
			Root<Student> rootStudent = criteriaQuery.from(Student.class);
			
			criteriaQuery.orderBy(criteriaBuilder.desc(rootStudent.get("age")));


			CriteriaQuery<Student> select = criteriaQuery.select(rootStudent);
			TypedQuery<Student> query = em.createQuery(select);
			query.getResultList().stream().forEach(s->System.out.println(s));
			
			em.getTransaction().commit();

		} catch (Exception e) {
			System.out.println(e.toString());
		}
		em.close();
		entityFactory.close();
	}
	
	public void criteriaGroupByAge() {
		EntityManagerFactory entityFactory = JPAUtil.getEntityManagerFactory();
		EntityManager em = entityFactory.createEntityManager();
		try {
			em.getTransaction().begin();
			CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
			CriteriaQuery<Object[]> criteriaQuery = criteriaBuilder.createQuery(Object[].class);
			Root<Student> rootStudent = criteriaQuery.from(Student.class);
			
			criteriaQuery.multiselect(rootStudent.get("name"), criteriaBuilder.count(rootStudent)).groupBy(rootStudent.get("name")); 
			List<Object[]> studentList = em.createQuery(criteriaQuery).getResultList();
			
			System.out.print(" StudentName \t Count\n"); 
			for(Object[] object : studentList) {
				System.out.println(object[0] + "\t" + object[1]);
			}
//			.stream().forEach(object -> System.out.println(object[0] + "\t" + object[1]));
			em.getTransaction().commit();

		} catch (Exception e) {
			System.out.println(e.toString());
		}
		em.close();
		entityFactory.close();
	}

}
