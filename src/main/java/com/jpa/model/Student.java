package com.jpa.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * The persistent class for the student database table.
 * 
 */
@Entity
@NamedQueries({ 
	@NamedQuery(name = "findAll", query = "SELECT s FROM Student s"),
	@NamedQuery(name = "findByName", query = "SELECT s FROM Student s WHERE s.name = :studentName"),
	@NamedQuery(name = "findById", query = "SELECT s FROM Student s WHERE s.id > :i1 and s.id < :i2")})

@NamedStoredProcedureQuery(
	    name = "findByAgeProcedure", 
	    procedureName = "getStudentByAge", 
	    resultClasses = { Student.class}, 
	    parameters = { 
	        @StoredProcedureParameter (name = "s_age", type = Integer.class, mode = ParameterMode.IN),
	        @StoredProcedureParameter (name = "s_id", type = Integer.class, mode = ParameterMode.IN),
	        @StoredProcedureParameter (name = "p_name", type = String.class, mode = ParameterMode.OUT)})
public class Student implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	private String name;
	
	private int age;

	public Student(String name, int age, Profile proflie) {
		super();
		this.name = name;
		this.age = age;
		this.profile = proflie;
	}

	@OneToOne
	@JoinColumn(name = "profileId")
	private Profile profile;
	
	@OneToMany
	@JoinColumn(name = "studentId")
	private List<Address> addresses = new ArrayList<Address>();
	@ManyToMany
	@JoinTable(name = "student_groups", joinColumns = @JoinColumn(name = "studentId"),
	inverseJoinColumns = @JoinColumn(name = "groupId"))
	private Set<Group> groups = new HashSet<Group>();
	
	public Student(String name, Set<Group> groups) {
		super();
		this.name = name;
		this.groups = groups;
	}

	public Set<Group> getGroups() {
		return groups;
	}

	public void setGroups(Set<Group> groups) {
		this.groups = groups;
	}

	public Student(String name, int age, List<Address> addresses) {
		super();
		this.name = name;
		this.age = age;
		this.addresses = addresses;
	}

	public List<Address> getAddresses() {
		return addresses;
	}

	public void setAddresses(List<Address> addresses) {
		this.addresses = addresses;
	}

	public Student() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Student(String name, int age) {
		super();
		this.name = name;
		this.age = age;
	}
	
	

	public Student(String name, int age, Set<Group> groups) {
		super();
		this.name = name;
		this.age = age;
		this.groups = groups;
	}

	public Profile getProfile() {
		return profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	@Override
	public String toString() {
		return "Student{" +
				"id=" + id +
				", name='" + name + '\'' +
				", age=" + age +
				'}';
	}
}