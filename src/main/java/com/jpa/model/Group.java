package com.jpa.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * The persistent class for the group database table.
 * 
 */
@Entity
@Table(name = "groups")
//@NamedQueries({ 
//	@NamedQuery(name = "findAll", query = "SELECT g FROM Group g"),
//	@NamedQuery(name = "findByName", query = "SELECT g FROM Group g WHERE g.name = :groupName"),
//	@NamedQuery(name = "findById", query = "SELECT g FROM Group g WHERE g.id > :i1 and g.id < :i2")})

@NamedStoredProcedureQuery(
	    name = "findByIdProcedure", 
	    procedureName = "getGroupById", 
	    resultClasses = { Group.class }, 
	    parameters = { 
	        @StoredProcedureParameter (name = "ID1", type = Integer.class, mode = ParameterMode.IN),
	        @StoredProcedureParameter(name = "ID2", type = Integer.class, mode = ParameterMode.IN)})
public class Group implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	private String name;

	@ManyToMany(mappedBy = "groups")
	private Set<Student> students = new HashSet<>();

	public Group(String name, Set<Student> students) {
		super();
		this.name = name;
		this.students = students;
	}

	public Set<Student> getStudents() {
		return students;
	}

	public void setStudents(Set<Student> students) {
		this.students = students;
	}

	public Group() {
		super();
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public Group(String name) {
		super();
		this.name = name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return id + " " + name;
	}

}