package com.jpa.util;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JPAUtil {
	public static final String PERSISTANCE_UNIT_NAME = "JPA-herbinate";
	public static EntityManagerFactory getEntityManagerFactory() {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory(PERSISTANCE_UNIT_NAME);
		return factory;
	}
}
